import React from 'react';
import styled, { css } from 'styled-components';
import { prop, ifProp } from 'styled-tools';

const HeaderWrapper = styled.header`
	width: 100%;
	color: ${prop('theme.colors.textMain')};
`;

const LogoSection = styled.section`
	background-color: ${prop('theme.colors.main')};
	padding: .75em 2em;
`;

const Logo = styled.h3`
	margin: 0;
`;

const FindJob = styled.section`
	background-color: ${prop('theme.colors.secondary')};
	padding: 3em 2em;
	border-top: 2px solid ${prop('theme.colors.borderColor')};
	border-bottom: 2px solid ${prop('theme.colors.borderColor')};
`;

const SecondaryHeading = styled.h2`
	margin: 0;
`;

const FiltersSection = styled.section`
	display: flex;
	background-color: ${prop('theme.colors.secondary')};
	padding: 0 2em;
`;

const Filter = styled.article`
	padding: .75em;
	margin-right: 3em;

	${ifProp(
		'active',
		css`
			border-bottom: .5em solid ${prop('theme.colors.underlineColor')};
			margin-top: .5em;
		`,
		css`
		margin-top: .5em;
		margin-bottom: .5em;
		`
	)};
`;

const Header = () => (
	<HeaderWrapper>
		<LogoSection>
			<Logo>Rabotify</Logo>
		</LogoSection>
		<FindJob>
			<SecondaryHeading>Find a Job</SecondaryHeading>
		</FindJob>
		<FiltersSection>
			<Filter active>
				<span>Latest</span>
			</Filter>
			<Filter>
				<span>Remote</span>
			</Filter>
			<Filter>
				<span>Contract</span>
			</Filter>
			<Filter>
				<span>Employers</span>
			</Filter>
		</FiltersSection>
	</HeaderWrapper>
);

export default Header;
