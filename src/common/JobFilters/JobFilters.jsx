import React from 'react';
import { compose, pure } from 'recompose';
import { prop } from 'styled-tools';
import styled from 'styled-components';

const Filters = styled.section`
	padding: 0.75em 2em;
`;

const FilterBlock = styled.div`
  padding: .5em 0;
`;

const FilterCategory = styled.h5`
	color: ${prop('theme.colors.dimmed')};
	margin: 0.5em 0;
`;

const Checkbox = styled.div``;

const JobFilters = () => (
	<Filters>
		<FilterBlock>
			<FilterCategory>Experience</FilterCategory>
			<Checkbox>Internships</Checkbox>
			<Checkbox>Entry-level</Checkbox>
			<Checkbox>Experienced</Checkbox>
		</FilterBlock>
		<FilterBlock>
			<FilterCategory>Remote</FilterCategory>
			<Checkbox>Work from Home</Checkbox>
			<Checkbox>Work from Anywhere</Checkbox>
		</FilterBlock>
		<FilterBlock>
			<FilterCategory>Job Type</FilterCategory>
			<Checkbox>Full-time</Checkbox>
			<Checkbox>Part-time</Checkbox>
			<Checkbox>Contract</Checkbox>
		</FilterBlock>
	</Filters>
);

export default compose(pure)(JobFilters);
