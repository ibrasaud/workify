module.exports = {
	colors: {
		main: '#1074FC',
		borderColor: '#0E62D5',
		underlineColor: '#0E53B1',
		dimmed: '#999',
		secondary: '#0F69E3',
		textMain: '#FFF'
	}
};
