import React from 'react';
import styledNormalize from 'styled-normalize';
import { ThemeProvider, injectGlobal } from 'styled-components';

import Header from './common/Header/Header';
import ContentSection from './ContentSection/ContentSection';
import theme from './theme';

injectGlobal`
	${styledNormalize}

	h1, h2, h3, h4, h5, span, p {
		font-family: 'Inter UI';
	}

	*,
	*:before,
	*:after {
			box-sizing: border-box;
	}
`;

const App = () => (
	<ThemeProvider theme={theme}>
		<React.Fragment>
			<Header />
			<ContentSection />
		</React.Fragment>
	</ThemeProvider>
);

export default App;
