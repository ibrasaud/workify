import React, { Component } from 'react';

import JobFilters from '../common/JobFilters/JobFilters';

class ContentSection extends Component {
	render() {
		return (
			<section>
				<JobFilters />
			</section>
		);
	}
}

export default ContentSection;
